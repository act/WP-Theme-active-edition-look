<?php
/**
 * The left sidebar for our theme
 *
 * Displays the selected widgets
 *
 * @package University of Reading
 */
 ?>


	<?php if ( is_active_sidebar( 'sidebar-left' ) ) : ?>
		<?php dynamic_sidebar( 'sidebar-left' ); ?>
	<?php endif; ?>
	
	
