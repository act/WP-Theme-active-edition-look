<?php
/**
 * University of Reading Active Edition theme functions and definitions.
 *
 * @package University of Reading 
 */
 
 
 /***************************************************************************/
//                     POSTS FUNCTIONALITIES
/***************************************************************************/
/***************************************************************************/
// Add default posts and comments RSS feed links to head. */
add_theme_support( 'automatic-feed-links' );

/***************************************************************************/
//Add pictures to the RSS feeds  */
function featuredtoRSS($content) {
global $post;
if ( has_post_thumbnail( $post->ID ) ){
$content = '<div>' . get_the_post_thumbnail( $post->ID, 'medium', array( 'style' => 'margin-bottom: 15px;' ) ) . '</div>' . $content;
}
return $content;
}
add_filter('the_excerpt_rss', 'featuredtoRSS');
add_filter('the_content_feed', 'featuredtoRSS');

/***************************************************************************
//Enable support for Post Thumbnails on posts and pages. */
add_theme_support( 'post-thumbnails', array( 'post' ) );

/***************************************************************************/
//Add author field for people pages (Campus Directory)*/
add_post_type_support( 'emd_person', 'author' );

/***************************************************************************/
//Sort the people search results by alphabetical order. 
//Note: Some pages (e.g. with taxonomies) will be sorted by Last Name in another function below
function sort_persons( $orderby, $query )
{ 	
	if ($_POST['form_name'] == 'people_search') {
        return "post_title ASC";
	}
    return $orderby;
}
add_filter('posts_orderby', 'sort_persons', 10, 2);


/* Filtering of people search form to display by Last Name. 
As of 21/02/2019, Doesn't work when search by name, only by tags...
function join_person_lname($join) {
	if ( ( isset ($_POST['form_name']) ) && ( $_POST['form_name'] == 'people_search' ) ){
		remove_filter('posts_join','join_person_lname');
		global $wpdb;
		$join .= ' LEFT JOIN '.$wpdb->postmeta.' ON ( '.$wpdb->posts.'.ID = '.$wpdb->postmeta.'.post_id ) 
		LEFT JOIN '.$wpdb->postmeta.' AS mt1 ON (mt1.meta_key = "emd_person_lname" )';
	}
	return $join;
}
add_filter('posts_join','join_person_lname');

function where_person_lname($where, $query) {
	if ( ( isset ($_POST['form_name']) ) && ( $_POST['form_name'] == 'people_search' ) ){
		remove_filter('posts_where','where_person_lname');
		global $wpdb;
		$where .= ' AND ( '.$wpdb->postmeta.'.meta_key = "emd_person_lname" OR mt1.post_id IS NULL )' ;
	}
	return $where;
}
add_filter('posts_where','where_person_lname', 10, 2);

function orderby_person_lname($orderby, $query) {
	if ( ( isset ($_POST['form_name']) ) && ( $_POST['form_name'] == 'people_search' ) ){
		remove_filter('posts_orderby','orderby_person_lname');
		global $wpdb;
		$orderby = $wpdb->postmeta.'.meta_value ASC';
	}
	return $orderby;
}
add_filter('posts_orderby','orderby_person_lname', 10, 2);
*/


add_filter('posts_request','debug_post_request',11); // debugging sql query of a post
function debug_post_request($sql_text) {

   $GLOBALS['debugku'] = $sql_text; //intercept and store the sql<br/>
   return $sql_text; 

}


/*
Sort Campus Directory listings by Last Name. 
CAUTION: Filter will only work on hard-coded pages or taxonomies (see below)
*/
function order_people_surname( $query ) {
	// do not modify queries in the admin
	if( is_admin() ) {
		return $query;
	}
	$queried_object = get_queried_object();
	$taxonomy = $queried_object->taxonomy;
	$page_slug = $queried_object->post_name;
	
	//Test if we are displaying one of the pages we want to sort, then sort:
	if ( ($taxonomy == 'directory_tag') || ($taxonomy == 'person_location') || ($taxonomy == 'person_rareas') || ($taxonomy == 'person_title') || ($page_slug=='people-grid') || ($page_slug=='foyer-display') || ( strpos($page_slug, 'people') !== false ) ) {
		$query->set('orderby', 'meta_value');	
		//$query->set('meta_key', 'emd_person_lname');	
		$query->set('meta_query', 
			array(
				'relation' => 'OR',
				array( 
					'key'=>'emd_person_lname',
					'compare' => 'EXISTS'           
				),
				array( 
					'key'=>'emd_person_lname',
					'compare' => 'NOT EXISTS'           
				)
			));		
		$query->set('order', 'ASC'); 
	}
	// return 
	return $query;
}
add_action('pre_get_posts', 'order_people_surname', 9999);

//Change default Campus Directory capabilities for tags. Otherwise, they are not displayed for people other than superadmins*/
function update_directory_tag_args( $args, $taxonomy ) {
    if ( $taxonomy == 'directory_tag' ) {
        $args['capabilities'] =  array(
					'manage_terms' => 'manage_categories',
					'edit_terms' => 'manage_categories',
					'delete_terms' => 'manage_categories',
					'assign_terms' => 'edit_posts'
				);
    }
    return $args;
}
add_filter( 'register_taxonomy_args', 'update_directory_tag_args', 10, 2 );


//Make Campus Directory display all results and not only 10*/
function persons_remove_limit($query){
	$query->set('posts_per_page', '-1');
}
add_action('pre_get_posts', 'persons_remove_limit');

/***************************************************************************
 * Show all parents, regardless of post status.
 * Comes from here: https://www.mightyminnow.com/2014/09/include-privatedraft-pages-in-parent-dropdowns/
 */
function my_slug_show_all_parents( $args ) {
	$args['post_status'] = array( 'publish', 'pending', 'draft', 'private' );
	return $args;
}
add_filter( 'page_attributes_dropdown_pages_args', 'my_slug_show_all_parents' );
add_filter( 'quick_edit_dropdown_pages_args', 'my_slug_show_all_parents' );


/***************************************************************************/
//                     BACK-OFFICE EXTENSIONS
/***************************************************************************/

/***************************************************************************
//Add styles for tinyMCE editor */
add_editor_style('css/editor-style.css');


/***************************************************************************/
//Hide the Edit Profile menu in top bar. (So it is not mis-used with the people plugin)
function admin_bar_remove_profile() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('edit-profile');
 }
add_action('wp_before_admin_bar_render', 'admin_bar_remove_profile', 0);

/***************************************************************************
//Customizer for Colors*/
add_action('customize_register', 'theme_color_customizer');
function theme_color_customizer($wp_customize){
 //adding section in wordpress customizer   
$wp_customize->add_section('color_settings_section', array(
  'title'          => 'Theme colors'
 ));
//adding setting for color selection
$wp_customize->add_setting('color_settings', array(
 'default'        => 'green',
 'sanitize_callback' => 'sanitize_text_field',
 ));
$wp_customize->add_control('footer_control', array(
 'label'   => 'Select the color',
 'section' => 'color_settings_section',
 'settings' => 'color_settings',
 'type'    => 'select',
 'choices'    => array(
            'green' => 'Green',
            'orange' => 'Orange',
            'rubine' => 'Rubine',
            'purple' => 'Purple',
            'navy' => 'Navy',
            'red' => 'Red',
            'blue' => 'Blue',
            'teal' => 'Teal',
        ),
));
}


/***************************************************************************
 * Register a sidebar widget area.
 *
 * copied from Twenty Fourteen 1.0
 */
function UoR_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Right sidebar', 'UoR_research' ),
		'id'            => 'sidebar-right',
		'description'   => __( 'Boxes that appear on the right, below page-based boxes', 'UoR_research' ),
		'before_widget' => '<div class="pullout-box">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Left sidebar', 'UoR_research' ),
		'id'            => 'sidebar-left',
		'description'   => __( 'Boxes that appear on the left, below other page-based boxes', 'UoR_research' ),
		'before_widget' => '<div class="pullout-box">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'UoR_widgets_init' );

/************************************************
 * Hide the Add New User page
 */
function custom_menu_page_removing() {
    remove_submenu_page( 'users.php','user-new.php' );
}
add_action( 'admin_menu', 'custom_menu_page_removing' );

// Hide button to add user on users list page
function this_screen() {
    $current_screen = get_current_screen();
    if( $current_screen->id === "users" ) {
		echo '<style type="text/css">
		.page-title-action { display:none; }
		</style>';
    }
}
add_action( 'current_screen', 'this_screen' );

/***************************************************************************/
// This is PHP function to convert a user-supplied URL to just the domain name,
// which I use as the link text. (https://gist.github.com/davejamesmiller/1965937)
// Remember you still need to use htmlspecialchars() or similar to escape the
// result. */
function url_to_domain($url)
{
    $host = @parse_url($url, PHP_URL_HOST);

    // If the URL can't be parsed, use the original URL
    // Change to "return false" if you don't want that
    if (!$host)
        $host = $url;

    // The "www." prefix isn't really needed if you're just using
    // this to display the domain to the user
    if (substr($host, 0, 4) == "www.")
        $host = substr($host, 4);

    return $host;
}

/***************************************************************************/
//Function to check that the provided URL for external page load is from the University domain. If not, it is not loaded.
function include_external_page ($pageURL){
	if (substr(url_to_domain($pageURL), -13)!='reading.ac.uk'){
		echo "This page is trying to load an unauthorized external resource...";
	}
	else {
		$headers = @get_headers($pageURL);
		if(strpos($headers[0],'404') === false)
		{
		  echo file_get_contents($pageURL); 
		}
		else
		{
		  echo "ERROR: The page cannot be displayed (Trying to load: ".$pageURL.")";
		}		
	}
}




/***************************************************************************/
//                     DISPLAY
/***************************************************************************/

/***************************************************************************
//This theme has 1 menu location, on the left */
function add_menu()
{
	register_nav_menu( 'primary', __( 'Primary Menu', 'UniversityOfReading' ) );
}
add_action('init', 'add_menu');

/***************************************************************************
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
add_theme_support( 'html5', array(
	'search-form',
	'comment-form',
	'comment-list',
	'gallery',
	'caption',
) );
add_theme_support( 'title-tag' );



/***************************************************************************
 * Build menu. Display only parents and siblings, or level 1 items if page is not listed in menu
 */
add_filter( 'wp_nav_menu_objects', 'only_submenu_for_current', 10, 2 );
function only_submenu_for_current( $sorted_menu_items, $args ) {
    $parent_to_keep = array( 0 );
    foreach ( $sorted_menu_items as $item ) { //loop on all menu items
		if ( (count( array_intersect( $item->classes, array( //if menu item is an ancestor of current page, we'll keep it
               'current-menu-item',
               'current-menu-parent',
               'current-menu-ancestor'	) ) ) > 0 ) 
			   || ( $item->menu_item_parent == 0 ) ) //or if the item is from the first level, we'll keep it too.
		{
			//We can keep this item
			$parent_to_keep[] = $item->ID; //we build the array of alligible parents
		} 
    }

    // 2nd pass
    foreach ( $sorted_menu_items as $key => $item ) { //loop again on all elements to check their parents and remove them if necessary
		if ( ! in_array( $item->menu_item_parent, $parent_to_keep ) ) { //We check if the parent of the item is elligible. If not, we don't display it
			unset( $sorted_menu_items[ $key ] );
		}
    }
    return $sorted_menu_items;
}

/***************************************************************************/
/* Filter to limit the size of the page titles*/
add_filter('the_title', 'truncate_long_title');
function truncate_long_title($title)
{
    if (strlen($title) > 70) {
        $title = substr($title, 0, 70).'...';
    }
    return $title;
}

/***************************************************************************/
/* max size of the pictures / elements in articles (may not be actually taken into account by this theme, but requested for standardisation) */
if ( ! isset( $content_width ) ) {
	$content_width = 730;
}

/***************************************************************************/
/* Functions to highlight search results: */
function search_excerpt_highlight() {
    $excerpt = get_the_excerpt();
    $keys = preg_quote(implode('|', explode(' ', get_search_query())));
    $excerpt = preg_replace('/(' . $keys .')/iu', '<strong class="search-highlight">\0</strong>', $excerpt);

    echo '<p>' . $excerpt . '</p>';
}
function search_title_highlight() {
    $title = get_the_title();
    $keys = preg_quote(implode('|', explode(' ', get_search_query())));
    $title = preg_replace('/(' . $keys .')/iu', '<strong class="search-highlight">\0</strong>', $title);

    echo $title;
}