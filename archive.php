<?php
/**
 * Display list of posts for a particular category
 *
 * @package University of Reading
 */
get_header();
?>


<div class="content-center" id="main-content">
  <!-- PAGE HEADER -->
  <div class=" c-<?php echo get_theme_mod( 'color_settings');?> "><h1 class="page-header"><?php single_cat_title();?> posts</h1></div>
<!-- CONTENT START -->
    <!-- LEFT COLUMN -->
      <div class="cl-side_2l c-<?php echo get_theme_mod( 'color_settings');?>" id="page-menu-hold">
		<div class="left-menu-list">
				  <!-- HOME MENU BUTTON -->
				  <ul id="subNav">
					<li><a class="left-menu-home" href="https://www.reading.ac.uk/" accesskey="1" title="University of Reading Home Page"><span>UoR Home</span></a></li>
				  </ul>
						
			<ul id='ae_menu'>
				<?php 
				// Display the same menu as the frontpage
				$id_page= get_option('page_on_front'); //by default, menu of the current page
				$menuname=get_field('specificmenu',$id_page); //get the name of the menu to be displayed
				wp_nav_menu( array( 'menu' => $menuname, 'theme_location' => 'primary', 'menu_class' => 'ae_menu', 'depth' => '3' ) );
				?>
			</ul>
		</div>
<!--SubMenu Bar END-->	
	</div> 



    <!-- MAIN COLUMN -->
      <div class="cl-main_2 c-<?php echo get_theme_mod( 'color_settings');?>">
        <div class="content-body">
			<?php
			if (have_posts()) : ?>
				<div class="post-category"><table border="0">
			   <?php while (have_posts()) :
				  the_post();?><tr>
				  <td><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(151,151)  ); ?></a></td>
				  <td><?php the_title( sprintf( '<h2><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?></td>
				  <td><small><?php the_time('F jS, Y'); ?></small></td>
				  </tr>
					<?php //echo "<br><br>---<br>";
					 //the_content();
			   endwhile; ?></table></div> <?php
			else: 
				echo "<p>Sorry, no posts matched your criteria.</p>";
			endif;
			?>

        </div>
      </div>	  
		<?php if (is_active_sidebar( 'sidebar-right' ))  //If a right box has been defined in the widgets, we'll display it
			{
				?>
				<!-- RIGHT SIDE -->
				  <div class="cl-side_2r c-<?php echo get_theme_mod( 'color_settings');?>" id="right-pullout-hold">
				  <?php
						get_sidebar('right');
					?>
				  </div>
				  <?php
			} ?>
      <div class=" c-<?php echo get_theme_mod( 'color_settings');?> " id="right-pullout-switch">
      </div>

    </div>
    <div class="colour-band c-<?php echo get_theme_mod( 'color_settings');?> colour-band-show_ ">
      <div class="colour-band-container">
        <div class="colour-band-center">
          <h3></h3>
			<p></p>
        </div>
      </div>
    </div>

<?php
get_footer(); ?>



