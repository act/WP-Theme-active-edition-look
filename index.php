<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package University of Reading
 */
get_header();
?>


<div class="content-center" id="main-content">
  <!-- PAGE HEADER -->
  <div class=" c-<?php echo get_theme_mod( 'color_settings');?> "><h1 class="page-header"><?php echo the_title(); ?></h1></div>
<!-- CONTENT START -->
    <!-- LEFT COLUMN -->
      <div class="cl-side_2l c-<?php echo get_theme_mod( 'color_settings');?>" id="page-menu-hold">
<div class="left-menu-list">
	      <!-- HOME MENU BUTTON -->
          <ul id="subNav">
            <li><a class="left-menu-home" href="https://www.reading.ac.uk/" accesskey="1" title="University of Reading Home Page"><span>UoR Home</span></a></li>
          </ul>
				
	<ul id='ae_menu'>
		<?php 
		// Find the menu to display. Either the one selected or the one inherited
		$id_page=get_the_ID(); //by default, menu of the current page
		if (get_field('specificmenu') == 'inheritmenu')
		{
			// loop into parent pages to find the closest specific menu
					$ancestors = get_ancestors( get_the_ID(), 'page' ); //get list of parent pages
					foreach ($ancestors as $ancestor) //find the closest parent with a left box title
					{
						if (get_field('specificmenu',$ancestor) != "inheritmenu") 
						{ 
							$id_page=$ancestor; //get id of parent page with the menu
							break;
						}
					}
		}
		$menuname=get_field('specificmenu',$id_page); //get the name of the menu to be displayed
		wp_nav_menu( array( 'menu' => $menuname, 'theme_location' => 'primary', 'menu_class' => 'ae_menu', 'depth' => '3' ) );
		?>
	</ul>
</div>
<!--SubMenu Bar END-->

		<?php  //Display the  left box if any (either left box or highlight)
		if ((get_field('leftboxtitle') != "") || (get_field('leftboxinherit') == "true") || (get_field('highlighttitle') != "") || (get_field('highlightinherit') == "true"))
		{ ?>
			<div id="kicker-seemore-box">
			<?php if ((get_field('leftboxtitle') != "") || (get_field('leftboxinherit') == "true") ) //if we want to display a left box
			{
				$id_page=get_the_ID(); //by default, we'll display the box for current page. But this id will be updated to parent page if inherit has been selected
				if (get_field('leftboxinherit') == "true") 
				{
					$ancestors = get_ancestors( get_the_ID(), 'page' ); //get list of parent pages
					foreach ($ancestors as $ancestor) //find the closest parent with a left box title
					{
						if (get_field('leftboxtitle',$ancestor) != "") 
						{ 
							$id_page=$ancestor; //get id of parent page
							break;
						}
					}
				}
			?>
				<!-- SEE MORE -->
				<div class="pullout-box" id="left-pullout-box">
				<h3><?php the_field('leftboxtitle',$id_page); ?></h3>
				<?php the_field('leftboxtext',$id_page); ?>
				</div>
			<?php }
		
			if ((get_field('highlighttitle') != "") || (get_field('highlightinherit') == "true")) 
			{ //if we want to display a highlight box 
				$id_page=get_the_ID(); //by default, we'll display the box for current page. But this id will be updated to parent page if inherit has been selected
				if (get_field('highlightinherit') == "true") 
				{
					$ancestors = get_ancestors( get_the_ID(), 'page' ); //get list of parent pages
					foreach ($ancestors as $ancestor) //find the closest parent with a left box title
					{
						if (get_field('highlighttitle',$ancestor) != "") 
						{ 
							$id_page=$ancestor; //get id of parent page
							break;
						}
					}
				}	?>
					
				<!-- KICKER -->
				  <div class="kicker-box" id="kicker-box">
					<h3><p style="COLOR: #ffffff"><?php the_field('highlighttitle',$id_page); ?></p></h3>
					<h2 style="COLOR: #ffffff"><?php the_field('highlighttext',$id_page); ?></h2>
				  </div>
			<?php } ?>
			</div>
		<?php } ?>
		
				<?php
					get_sidebar('left');
				?>
		</div> 



    <!-- MAIN COLUMN -->
      <div class="cl-main_2 c-<?php echo get_theme_mod( 'color_settings');?>">
        <div class="content-body">
		
			<?php
			if (have_posts()) :
			   while (have_posts()) :
					the_post();
					the_post_thumbnail( array(200,200)  );
					the_content();
			   endwhile;
			endif;
			
			
			//include external page if any
			if (get_field('include_page')!="") {//echo file_get_contents(get_field('include_page')); 
				include_external_page (get_field('include_page'));
			}
			?>
        </div>
      </div>

	<?php if ((get_field('rightboxtitle') != "") || (get_field('rightboxinherit') == "true"))  //If a right box has been defined, we'll display it
	{
		$id_page=get_the_ID(); //by default, we'll display the box for current page. But this id will be updated to parent page if inherit has been selected
				if (get_field('rightboxinherit') == "true") 
				{
					$ancestors = get_ancestors( get_the_ID(), 'page' ); //get list of parent pages
					foreach ($ancestors as $ancestor) //find the closest parent with a left box title
					{
						if (get_field('rightboxtitle',$ancestor) != "") 
						{ 
							$id_page=$ancestor; //get id of parent page
							break;
						}
					}
				}	
		?>
		<!-- RIGHT SIDE -->
		  <div class="cl-side_2r c-<?php echo get_theme_mod( 'color_settings');?>" id="right-pullout-hold">
			  <div class="pullout-box">
				<h3><?php the_field('rightboxtitle',$id_page);?></h3>
				<?php the_field('rightboxtext',$id_page);?>
			  </div>			
		  <?php
				get_sidebar('right');
			?>
		  </div>
		  <?php
	} ?>	  

      <div class=" c-<?php echo get_theme_mod( 'color_settings');?> " id="right-pullout-switch">
      </div>

    </div>
    <div class="colour-band c-<?php echo get_theme_mod( 'color_settings');?> colour-band-show_ ">
      <div class="colour-band-container">
        <div class="colour-band-center">
          <h3></h3>
			<p></p>
        </div>
      </div>
    </div>

<?php
get_footer(); ?>



