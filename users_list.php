<?php
/**
 * Displays the list of users of a particular website.
 * Can be used to display a list of staff for instance. As long as all staff are users of the wordpress site.
 *
 * @package University of Reading
 */
class Users_list_widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct('users_list', 'UsersList', array('description' => 'List of users.'));
    }
    
    public function widget($args, $instance)
    {
        $blogusers = get_users(  );
				// Array of WP_User objects.
				
				echo "<table><tr><th>Name</th><th>Website</th><th>Room</th></tr>";
				foreach ( $blogusers as $user ) {
					echo  "<tr><td><a href=\"". esc_html( $user->user_url )."\">". esc_html( $user->first_name ).' '. esc_html( $user->last_name ) . "</a></td><td><a href=\"". esc_html( $user->user_url )."\">" . esc_html( $user->user_url )."</a></td><td>". esc_html( $user->room ).'</td></tr>';
				}
				echo "</table>";
    }
}