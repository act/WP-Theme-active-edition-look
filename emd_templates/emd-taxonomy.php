<?php
if ( ! defined( 'ABSPATH' ) ) {
        exit; // Exit if accessed directly
}

get_header('emdplugins');
$container = apply_filters('emd_change_container','container','campus_directory', 'taxonomy');  
$queried_object = get_queried_object();
?>
<div class="content-center menu_no-right" id="main-content">
	<!-- PAGE HEADER -->
	<div class=" c-<?php echo get_theme_mod( 'color_settings');?> ">
		<h1 class="page-header"><?php echo get_queried_object()->name;?></h1>
	</div>
<!-- CONTENT START -->
    <!-- LEFT COLUMN -->
	<div class="cl-side_2l c-<?php echo get_theme_mod( 'color_settings');?>" id="page-menu-hold">
		<div class="left-menu-list">
			<!-- HOME MENU BUTTON -->
			<ul id="subNav">
			<li><a class="left-menu-home" href="https://www.reading.ac.uk/" accesskey="1" title="University of Reading Home Page"><span>UoR Home</span></a></li>
			</ul>
						
			<ul id='ae_menu'>
				<?php 
				wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'ae_menu', 'depth' => '3' ) );
				?>
			</ul>
		</div>
	</div>
<!--SubMenu Bar END-->   

<!-- MAIN COLUMN -->
<div class=" cl-main_2 c-<?php echo get_theme_mod( 'color_settings');?>">
	<div class="content-body">

	<table class="emd-person-table"> 
		<thead>
			<tr>
			<td><strong><strong>Name</strong></strong></td>
			<td><strong>Photo</strong></td>
			<td><strong>Role</strong></td>
			<td><strong>Tel. / Room No.</strong></td>
			<td><strong>Email</strong></td>
			</tr>
		</thead>
		<tbody>
		<?php
		while ( have_posts() ) : the_post(); ?>
				<tr id="post-<?php the_ID(); ?>" >
				<?php emd_get_template_part('campus-directory', 'taxonomy', str_replace("_","-",$queried_object->taxonomy . '-' . $post->post_type)); ?>
				</tr>
		<?php endwhile; // end of the loop. ?>
		</tbody>
	</table>
<?php	$has_navigation = apply_filters( 'emd_show_temp_navigation', true, 'campus_directory', 'taxonomy');
	if($has_navigation){	
		global $wp_query;
		$big = 999999999; // need an unlikely integer

?>
		<nav role="navigation" id="nav-below" class="site-navigation paging-navigation">
		<h3 class="assistive-text"><?php esc_html_e( 'Post navigation', 'wpas' ); ?></h3>

	<?php	
		if ( $wp_query->max_num_pages > 1 ) { ?>

		<?php $pages = paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var( 'paged' ) ),
			'total' => $wp_query->max_num_pages,
			'type' => 'array',
			'prev_text' => wp_kses( __( '<i class="fa fa-angle-left"></i> Previous', 'wpas' ), array( 'i' => array( 
			'class' => array() ) ) ),
			'next_text' => wp_kses( __( 'Next <i class="fa fa-angle-right"></i>', 'wpas' ), array( 'i' => array( 
			'class' => array() ) ) )
		) );
		if(is_array($pages)){
			$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
			echo '<div class="pagination-wrap"><ul class="pagination">';
			foreach ( $pages as $page ) {
				$paging_html = "<li";
				if(strpos($page,'page-numbers current') !== false){
					$paging_html.= " class='active'";
				}
				$paging_html.= ">" . $page . "</li>";
				echo $paging_html;
			}
			echo '</ul></div>';
		}
		} ?>
		</nav>
<?php	
	}
?>
	</div>

</div>
</div>
<?php get_footer('emdplugins'); ?>
