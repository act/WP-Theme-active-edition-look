<?php $real_post = $post;
$ent_attrs = get_option('campus_directory_attr_list');
?>
<td>
	<a class="archive permalink font-bold" href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>">
	<?php 
		if (emd_get_tax_vals(get_the_ID() , 'person_title')<>"") {echo wp_strip_all_tags(emd_get_tax_vals(get_the_ID() , 'person_title'))." ";}
		echo get_the_title(); ?>
	</a>
</td>
<td>
	<a title="<?php echo get_the_title(); ?>" href="<?php echo get_permalink(); ?>"><?php if (get_post_meta($post->ID, 'emd_person_photo')) {
		$sval = get_post_meta($post->ID, 'emd_person_photo');
		$thumb = wp_get_attachment_image_src($sval[0], 'thumbnail');
		echo '<img class="emd-img thumb" src="' . $thumb[0] . '" width="' . $thumb[1] . '" height="' . $thumb[2] . '" alt="' . get_post_meta($sval[0], '_wp_attachment_image_alt', true) . '"/>';
	} ?></a>
</td>
<td>
	<?php echo get_field('bio_excerpt'); ?>
</td>
<td>
	<?php echo esc_html(emd_mb_meta('emd_person_phone')); 
	if (esc_html(emd_mb_meta('emd_person_office')) <> "") echo " / ".esc_html(emd_mb_meta('emd_person_office'));
	?>
</td>
<td>
	<a href="mailto:<?php echo antispambot(esc_html(emd_mb_meta('emd_person_email'))); ?>"><?php echo antispambot(esc_html(emd_mb_meta('emd_person_email'))); ?></a>
</td>