
<div class="content-center" id="main-content">
	<!-- PAGE HEADER -->
	<div class=" c-<?php echo get_theme_mod( 'color_settings');?> "><h1 class="page-header">
		<?php if (emd_is_item_visible('tax_person_title', 'campus_directory', 'taxonomy')) {
			echo wp_strip_all_tags(emd_get_tax_vals(get_the_ID() , 'person_title'))." "; 
		} ?>
	<?php echo the_title(); ?></h1>
	</div>
<!-- CONTENT START -->
    <!-- LEFT COLUMN -->
	<div class="cl-side_2l c-<?php echo get_theme_mod( 'color_settings');?>" id="page-menu-hold">
		<div class="left-menu-list">
			<!-- HOME MENU BUTTON -->
			<ul id="subNav">
			<li><a class="left-menu-home" href="https://www.reading.ac.uk/" accesskey="1" title="University of Reading Home Page"><span>UoR Home</span></a></li>
			</ul>
						
			<ul id='ae_menu'>
				<?php 
				wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'ae_menu', 'depth' => '3' ) );
				?>
			</ul>
		</div>
	</div>
<!--SubMenu Bar END-->


    <!-- MAIN COLUMN -->
      <div class="cl-main_2 c-<?php echo get_theme_mod( 'color_settings');?>">
		<div class="content-body">
			<?php if (emd_is_item_visible('tax_person_rareas', 'campus_directory', 'taxonomy')) {  
				echo "<h4>(".emd_get_tax_vals(get_the_ID() , 'person_rareas').")</h4>"; 
			} ?>
			<?php
			if (get_field('bio_excerpt')!="") echo "<p>".get_field('bio_excerpt')."</p>";
			/*if (emd_is_item_visible('ent_person_type', 'campus_directory', 'attribute')) { ?> 
				<p> <?php echo emd_get_attr_val('campus_directory', $post->ID, 'emd_person', 'emd_person_type'); ?> </p><?php 
			}*/
			if (emd_is_item_visible('ent_person_bio', 'campus_directory', 'attribute')) {
				echo emd_mb_meta('emd_person_bio'); 
			} 
			?>
			<?php if (emd_is_item_visible('ent_person_cv', 'campus_directory', 'attribute')) { ?> 
				<h3><?php _e('Curriculum Vitae', 'campus-directory'); ?></h3> 
				<?php
				$emd_mb_file = emd_mb_meta('emd_person_cv', 'type=file');
				if (!empty($emd_mb_file)) {
					foreach ($emd_mb_file as $info) {
						$fsrc = wp_mime_type_icon($info['ID']);
						?>
						 <a  href='<?php echo esc_url($info['url']); ?>' target='_blank' title='<?php echo esc_attr($info['title']); ?>'><img src='<?php echo esc_url($fsrc); ?>' title='<?php echo esc_attr($info['name']); ?>' width='48' height='64'/></a><span style='padding:2px;font-size:80%;'><?php echo $info['title']; ?></span>
						<?php
					}
				}
				?>
			<?php
			} 
			
			//display external content if field has been populated
			if (get_field('include_page')!="") {//echo file_get_contents(get_field('include_page')); 
				include_external_page (get_field('include_page'));
			}
			
			//display centaur publications if selected
			if ((get_field('display_centaur')==1)&&(emd_is_item_visible('ent_person_email', 'campus_directory', 'attribute'))) {
				$email = esc_html(emd_mb_meta('emd_person_email')); 
				$email = str_replace(".","=2E",$email);
				$email = str_replace("@","=40",$email);
				$includepage="http://centaur.reading.ac.uk/view/creators_cv/".$email.".include";
				echo "<h3>Centaur Publications</h3>";
				include_external_page ($includepage);
			}
			echo "<p><span style=\"padding-top: 20px; display: block; text-align:right; font-size:80%; font-style:italic;\">Last update: ".get_the_modified_date()."<span></p>";
			?>
		</div>
      </div>

	
		<!-- RIGHT SIDE -->
		<div class="cl-side_2r c-<?php echo get_theme_mod( 'color_settings');?>" id="right-pullout-hold">
			  <div class="pullout-box">
				<h3>Contact</h3>
				<?php 
				if (emd_is_item_visible('ent_person_photo', 'campus_directory', 'attribute')) { ?> 
                    <?php 
					if (get_post_meta($post->ID, 'emd_person_photo')) {
						$sval = get_post_meta($post->ID, 'emd_person_photo');
						$thumb = wp_get_attachment_image_src($sval[0], 'medium');
						echo '<img class="" src="' . $thumb[0] . '" alt="' . get_post_meta($sval[0], '_wp_attachment_image_alt', true) . '"/>';
					} 
				}
				if (emd_is_item_visible('ent_person_email', 'campus_directory', 'attribute')) { ?> 
                        <p><a href="mailto:<?php echo antispambot(esc_html(emd_mb_meta('emd_person_email'))); ?>"><?php echo antispambot(esc_html(emd_mb_meta('emd_person_email'))); ?></a> 	</p> <?php
				} 		
				if (emd_is_item_visible('ent_person_address', 'campus_directory', 'attribute')) { ?> 
                        <h4>Address:</h4><p>  <?php echo esc_html(emd_mb_meta('emd_person_address'));?></p><?php
				} 
				if (emd_is_item_visible('ent_person_office', 'campus_directory', 'attribute')) { ?> 
                        <h4>Office:</h4> <p> <?php echo esc_html(emd_mb_meta('emd_person_office')); if (emd_is_item_visible('tax_person_location', 'campus_directory', 'taxonomy')) {echo " - ".emd_get_tax_vals(get_the_ID() , 'person_location'); } ?></p><?php
				} 
				if (emd_is_item_visible('ent_person_phone', 'campus_directory', 'attribute')) { ?> 
                        <h4>Phone:</h4><p> <a href="tel:<?php echo esc_html(emd_mb_meta('emd_person_phone')); ?>"><?php echo esc_html(emd_mb_meta('emd_person_phone')); ?></p> <?php
				}					
				if (emd_is_item_visible('ent_person_website', 'campus_directory', 'attribute')) {?>
				<p class="segvalue"><a href='<?php echo esc_html(emd_mb_meta('emd_person_website')); ?>
				' target='_blank' title='<?php echo get_the_title(); ?>'>Group Website<?php // _e(esc_html(emd_mb_meta('emd_person_website')), 'campus-directory'); ?></a></p><?php } 
				if (emd_is_item_visible('ent_person_pwebsite', 'campus_directory', 'attribute')) {?>
				<p class="segvalue"><a href='<?php echo esc_html(emd_mb_meta('emd_person_pwebsite')); ?>
				' target='_blank' title='<?php echo get_the_title(); ?>'>Personal Website<?php // _e(esc_html(emd_mb_meta('emd_person_pwebsite')), 'campus-directory'); ?></a></p><?php } ?>
				
				<p>
					<?php 
					if (emd_is_item_visible('ent_person_linkedin', 'campus_directory', 'attribute')) { ?>
						<a class="social-icon linkedin animate" href="<?php echo esc_html(emd_mb_meta('emd_person_linkedin')); ?>" target="_blank"><i class="fa fa-linkedin fa-fw" style="font-size:20px"></i></a>
					<?php
					} 
					if (emd_is_item_visible('ent_person_twitter', 'campus_directory', 'attribute')) { ?>
						<a class="social-icon twitter animate" href="<?php echo esc_html(emd_mb_meta('emd_person_twitter')); ?>" target="_blank"><i class="fa fa-twitter fa-fw" style="font-size:20px"></i></a>
					<?php
					} 
					do_action('emd_vcard', 'campus_directory', 'emd_person', $post->ID); ?> 
					<div><?php do_action('emd_vcard', 'campus_directory', 'emd_person', $post->ID); ?></div>
				</p>
			</div>
		</div>
</div>