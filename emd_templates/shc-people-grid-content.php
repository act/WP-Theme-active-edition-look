
<div class="emdcol span_1_of_5">
    		
        <div class="emdlabel bottom"> 
			<a href="<?php echo get_permalink(); ?>" title="<?php echo esc_html(emd_mb_meta('emd_person_fname')); ?>">
			<?php if (get_post_meta($post->ID, 'emd_person_photo')) {
				$sval = get_post_meta($post->ID, 'emd_person_photo');
				$thumb = wp_get_attachment_image_src($sval[0], 'thumbnail');
				echo '<img class="emd-img thumb" src="' . $thumb[0] . '" width="' . $thumb[1] . '" height="' . $thumb[2] . '" alt="' . get_post_meta($sval[0], '_wp_attachment_image_alt', true) . '" style="display:block; margin-right:auto; margin-left:auto;"/>';
				} ?> 
			</a>
		</div>
		<div class="emd_person_summary">
			<a href="<?php echo get_permalink(); ?>" title="<?php echo esc_html(emd_mb_meta('emd_person_fname')); ?>">
				
				<div class="emdlabel bottom"><span style="font-size:1.1em; font-weight:600; "> <?php echo esc_html(emd_mb_meta('emd_person_fname')); ?> <?php echo esc_html(emd_mb_meta('emd_person_lname')); ?></span>
				</div>
				<?php if (emd_is_item_visible('ent_person_office', 'campus_directory', 'attribute')) { ?> 
					<div class="emdlabel  bottom"> <span class="dashicons dashicons-building"></span> <?php echo esc_html(emd_mb_meta('emd_person_office')); ?></div>
					<?php
				} ?>
			</a>
			<?php if (emd_is_item_visible('ent_person_phone', 'campus_directory', 'attribute')) { ?> 
				<div class="emdlabel  bottom"> <span class="dashicons dashicons-phone"></span> <a href="tel:<?php echo esc_html(emd_mb_meta('emd_person_phone')); ?>
				"><?php echo esc_html(emd_mb_meta('emd_person_phone')); ?></a> </div>
			<?php
			} ?>
		</div>
</div>