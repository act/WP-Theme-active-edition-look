<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package University of Reading
 */
?>


<!-- FOOTER -->
<div class="nojs-footer" id="nojs-footer">
  <ul>
    <h4>Site navigation</h4>
    <li><a href="/">Home</a></li>
    <li><a href="/ready-to-study">Study & Life</a></li>
    <li><a href="/research">Research</a></li>
    <li><a href="#top">Back to top</a></li>
  </ul>
</div>
<div class="footer-container">
  <div class="footer-panorama"> </div>
  <div class="footer-center">
    <div class="footer-column">
      <div class="footer-section">
        <h2 class="content-fourth-header">Quick links</h2>
        <ul class="arrow-list">
          <li><a href="https://www.reading.ac.uk/jobs">Jobs@Reading</a></li>
          <li><a href="https://www.reading.ac.uk/library">Library</a></li>
          <li><a href="https://www.reading.ac.uk/its">IT services</a></li>
          <li><a href="https://www.reading.ac.uk/careers">Student careers &amp; placements</a></li>
          <li><a href="http://www.sport.reading.ac.uk/">SportsPark</a></li>
          <li><a href="http://www.rusu.co.uk/">Students' Union</a></li>
        </ul>
      </div>
      <div class="footer-section">
        <h2 class="content-fourth-header">Key dates</h2>
        <ul class="arrow-list">
          <li><a href="https://www.reading.ac.uk/15/study/study-termdates.aspx">Term dates</a></li>
          <li><a href="https://www.reading.ac.uk/ready-to-study/study/open-days.aspx">Open days</a></li>
        </ul>
      </div>
    </div>
    <div class="footer-column">
      <div class="footer-section">
        <h2 class="content-fourth-header">Do it online</h2>
        <ul class="arrow-list">
          <li><a href="https://www.bb.reading.ac.uk/">Blackboard learn</a></li>
          <li><a href="https://www.risisweb.reading.ac.uk/">RISISweb Portal</a></li>
          <li><a href="https://www.reading.ac.uk/internal/crbt/crbt-timetabling/crbt-studenttimetabling/crbt-student-timetabling.aspx">Timetables</a></li>
          <li><a href="http://www.readingconnect.net/">readingConnect</a></li>
          <li><a href="https://www.reading.ac.uk/internal/readinglive/its-rl-home.aspx">readingLive</a></li>
          <li><a href="https://www.owamail.reading.ac.uk/">Outlook web email</a></li>
          <li><a href="https://www.reading.ac.uk/internal/campus-card/sac-home.aspx">Campus card</a></li>
          <li><a href="https://www.webpay.reading.ac.uk/studentpayments/">Pay fees</a></li>
          <li><a href="https://www.store.reading.ac.uk">Online store</a></li>
          <li><a href="https://www.store.rusu.co.uk/">RUSU gift shop</a></li>
        </ul>
      </div>
    </div>
    <div class="footer-column">
      <div class="footer-section">
        <h2 class="content-fourth-header">Courses</h2>
        <ul class="arrow-list">
         <li><a href="https://www.reading.ac.uk/ready-to-study/study">Undergraduate and postgraduate taught courses</a></li>
         <li><a href="https://www.reading.ac.uk/graduateschool/prospectivestudents/gs-our-research-areas.aspx">Postgraduate research</a></li>
       </ul>
      </div>
      <div class="footer-section">
        <h2 class="content-fourth-header">A&ndash;Z</h2>
        <ul class="arrow-list">
          <li><a href="https://www.reading.ac.uk/a-z/az-academic.aspx">Schools &amp; departments</a></li>
          <li><a href="https://www.reading.ac.uk/a-z/az-research.aspx">Research groups &amp; centres</a></li>
          <li><a href="https://www.reading.ac.uk/15/about/about-museums.aspx">Museums &amp; collections</a></li>
          <li><a href="https://www.reading.ac.uk/a-z/az-admin.aspx">Admin &amp; services</a></li>
          <li><a href="http://centaur.reading.ac.uk/">Publications</a></li>
        </ul>
      </div>
    </div>
    <div class="footer-column">
      <div class="footer-section">
        <h2 class="content-fourth-header">Information for&hellip;</h2>
        <ul class="arrow-list">
          <li><a href="https://www.reading.ac.uk/welcome">New students</a></li>
          <li><a href="https://www.reading.ac.uk/news-and-events/about-contactpress.aspx">Press &amp; media</a></li>
          <li><a href="https://www.reading.ac.uk/teachers-and-advisors">Teachers &amp; advisors</a></li>
          <li><a href="https://www.reading.ac.uk/careers/">Employers</a></li>
          <li><a href="https://www.reading.ac.uk/internal/imps/DataProtection/DataProtectionGuidelines/imps-d-p-disclosing-parents.aspx">Parents</a></li>
          <li><a href="https://www.reading.ac.uk/about/working-with-the-community.aspx">Community</a></li>
          <li><a href="https://www.reading.ac.uk/student">Students</a></li>
          <li><a href="https://www.reading.ac.uk/alumni">Alumni</a></li>
          <li><a href="https://www.reading.ac.uk/staff">Staff</a></li>
        </ul>
      </div>
    </div>
    <div class="footer-column">
      <div class="footer-section">
        <h2 class="content-fourth-header">Stay in touch</h2>
        <ul class="arrow-list">
          <li><a href="https://www.reading.ac.uk/15/about/about-contacts.aspx">Contact us</a></li>
          <li><a href="https://www.reading.ac.uk/a-z/az-staffsearch.aspx">Find a member of staff</a></li>
          <li><a href="https://www.reading.ac.uk/security-services/report-a-crime/sec-report-a-security-issue.aspx">Report something</a></li>
          <li><a href="mailto:content@reading.ac.uk">Website feedback</a></li>
        </ul>
      </div>
      <div class="footer-section">
        <h2 class="content-fourth-header">Find us</h2>
        <ul class="arrow-list">
          <li><a href="https://www.reading.ac.uk/about/visit-us.aspx">Maps &amp; directions</a></li>
        </ul>
      </div>

<div class="footer-section">
        <h2 class="content-fourth-header">Get connected</h2>
        <ul class="connect-list">
          <li><a class="connect-twitter" href="https://twitter.com/uniofreading">Twitter</a></li>
          <li><a class="connect-fb" href="https://www.facebook.com/theuniversityofreading">Facebook</a></li>
          <li><a class="connect-yt" href="http://www.youtube.com/user/UniofReading">YouTube</a></li>
          <li><a class="connect-instagram" href="http://www.instagram.com/uniofreading/">Instagram</a></li>
          <li><a class="connect-flickr" href="http://www.flickr.com/photos/universityofreading/">Flickr</a></li>
        </ul>
      </div>   
 </div>
  </div>
</div>
<div class="nojs-information" id="nojs-information">
  <p><span></span>We use Javascript to improve your experience on <a href="https://www.reading.ac.uk">reading.ac.uk</a>, but it looks like yours is turned off. Everything will still work, but it is even more beautiful with Javascript in action. Find out more about why and how to turn it back on <a href="http://www.enable-javascript.com/" target="_blank">here</a>. <br />We also use cookies to improve your time on the site, for more information please see our <a href='https://www.reading.ac.uk/15/about/about-privacy.aspx#cookies'>cookie policy</a>.</p>
</div>
<!--[if lt IE 9]>
  <div class="ie6-information" id="ie6-information">
    <p><span></span>
<![endif]-->
<!--[if lte IE 6]>
  At the University of Reading we make every effort to support as many of the older browsers as possible, but due to the low percentage of users on IE6, we've decided that it's time to stop on-going support. IE6 doesn&rsquo;t have the safety features available on more modern browsers.<br/>
<![endif]-->
<!--[if (gt IE 6)&(lt IE 9)]>
  Although we make every effort to support the browser you are using, it is important to note that you are not using the most up-to-date version. This affects safety as well as your experience on our website.</br>
<![endif]-->
<!--[if lt IE 9]>
  We strongly suggest you upgrade to a safer, more modern browser like <a href="http://google.com/chrome">Google Chrome</a>, <a href="http://mozilla.org">Firefox</a> or the latest version of <a href="http://windows.microsoft.com/en-GB/internet-explorer/download-ie/">Internet Explorer</a>.</p>
</div>
<![endif]-->
<div class="identity-container">
  <div class="identity-center">
    <div class='identity-device-hold'>
      <div class="identity-device">University of Reading</div>
    </div>
    <ul class="identity-list">
      <li><a href="https://www.reading.ac.uk/15/about/about-copyright.aspx">&copy; University of Reading</a>
      <li><a href="https://www.reading.ac.uk/internal/finance/formsguidesandpolicies/fcs-TheUniversitysCharitableStatus.aspx">Charitable status</a></li>
      <li><a href="https://www.reading.ac.uk/15/about/about-accessibility.aspx">Accessibility</a></li>
      <li><a href="https://www.reading.ac.uk/15/about/about-privacy.aspx">Privacy policy</a></li>
      <li><a href="https://www.reading.ac.uk/15/about/about-privacy.aspx#cookies">Cookies</a></li>
      <li><a href="https://www.reading.ac.uk/15/about/about-disclaimer.aspx">Disclaimer</a></li>
      <li><a href="https://www.reading.ac.uk/15/about/sitemap.aspx">Site map</a></li>
      <li><a href="#top">Back to top</a></li>
    </ul>
  </div>
</div>

  </div>
</div> <!-- close wrappers -->

<!-- MOBILE NAVIGATION -->
<!-- TO TOP (not mobile) -->
<a class="footer-toTop" href="#top">Back to top</a>

<!-- NAVIGATION MAIN LIST -->
  <ul class="mobile-navigation-main">
    <form class="mobile-search" action="https://www.reading.ac.uk/search/public/search" method="get">
      <input type="text" name="q" title="Search the University of Reading website" placeholder="Search reading.ac.uk"/>
      <input name="site" value="internal" title="Search is internal" type="hidden">
      <input name="output" value="xml_no_dtd" title="Output search as XML" type="hidden">
      <input name="client" title="Search is internal" value="internal" type="hidden">
      <input name="proxystylesheet" title="Search will output to rdg stylesheets" value="internal" type="hidden">
      <button value="search" type="submit">Search</button>
    </form>
    <li class="nav-mob-home"><a href="https://www.reading.ac.uk">Home</a></li>
    <li class="c-study"><a href="https://www.reading.ac.uk/ready-to-study">Study & Life</a></li>
     <li class="c-research"><a href="https://www.reading.ac.uk/research">Research</a></li>
    <li class="c-business"><a href="https://www.reading.ac.uk/working-with-business">Business</a></li>
    <li class="c-about"><a href="https://www.reading.ac.uk/about">About us</a></li>
    <li class="c-news"><a href="https://www.reading.ac.uk/news-and-events">News &amp; events</a></li>
  </ul>

<!-- SCRIPTS -->
<!-- Modernizr - SVG & touch -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/rdg_modernizr-svg-touch.min.js"></script>


<!-- General JS snippets --> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/rdg_javascript.js"></script>
<?php wp_footer(); ?>
</body>
</html>