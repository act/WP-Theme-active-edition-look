<?php
/**
Template Name: No Template
 * This is the template that displays pages without any banner, nor sidebar. Only the page content.
 *
 * Created by University of Reading (E. Mathieu) on 9/01/2018
 */

get_header('notemplate');

?>

<div class="row bg-light-grey">
    <div class="paddingtop">
      
        <div class="row-medium masonry-area">
			<div class="pane base12 t-base6 pane-around clearfix" >      
				<article class="bg-white pad-around">
				<?php
				while (have_posts()) :
					the_post();
					
					the_content();

				endwhile; 	
				if (get_field('include_page')!="") {echo file_get_contents(get_field('include_page')); }
							
				
				?>
				</article>
				
			</div>
			
        </div>


    </div>
</div>
<?php
if (get_field('include_page')!="") {echo file_get_contents(get_field('include_page')); }

get_footer('notemplate'); ?>